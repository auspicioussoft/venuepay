<?php
session_start();
require '../vendor/autoload.php';
use Osiset\BasicShopifyAPI;
require '../conf.php';
require '../connect.php';
require '../tempoeConfig.php';

date_default_timezone_set('UTC');

/*function vpost($url, $data, $x_url_complete) {
	$curl = curl_init();
	curl_setopt_array($curl, array(
	  CURLOPT_URL => $url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => $data,
	  CURLOPT_HTTPHEADER => array(),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	  echo '<script>window.location.href = "'. $x_url_complete .'"</script>';
	}	
}

$x_url_callback = "https://custom-payment-test.myshopify.com/services/ping/notify_integration/venuepay/$endURL";
$TRADENO = str_replace("#","",$response->body->checkout->name);
$key = 'iU44RWxeik';
$message = 'Venue Payment Gateway';
$status = 'pending';

$array = array(
	'x_account_id' => $tempoe->RetailLocID,
	'x_reference' => $TRADENO,
	'x_currency' => $response->body->checkout->currency,
	'x_test' => $tempoe->test,
	'x_amount' => $response->body->checkout->total_price,
	'x_result' => $status,
	'x_gateway_reference' => $TRADENO,
	'x_message' => $message,
	'x_timestamp' => str_replace(' ','T',date('Y-m-d H:i:s'))."Z",
);
ksort($array);
$string = '';
foreach($array as $k => $v){
	$string .= $k.$v;
}
$array['x_signature'] =  hash_hmac('sha256', $string, $key);

$postFields = http_build_query($array);
vpost($x_url_callback, $postFields, $x_url_complete);*/

if(isset($_REQUEST['shop'])) {
    $responseData = explode('?key=',$_REQUEST['shop']);
	$resData = explode('?endURL=',$responseData[1]);
    $shop = $responseData[0];
    $checkoutKey = $resData[0];
	$endURL = $resData[1];
    // API
    $api = new BasicShopifyAPI();
    $api->setVersion('unstable');
    $api->setShop($shop);
    $api->setApiKey(SHOPIFY_API_KEY);
    $api->setApiSecret(SHOPIFY_API_SECRET);
	//Access Token
    $select = "SELECT * FROM $tableName WHERE shop='$shop'";
    $result = mysqli_query($con, $select);
    if(mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
		$access_token = $row['access_token'];
        $api->setAccessToken($access_token);
        //checkout API
		$response = $api->rest("GET", "/admin/checkouts/$checkoutKey.json");
        echo '<pre>';

		$x_url_complete = "https://$shop/$endURL/checkouts/$checkoutKey/offsite_gateway_callback";
		
        $checkout = json_decode(json_encode($response->body->checkout), true);
        $orderData = array (
            'order' => array (
                'note_attributes' => 
                array (
                    array (
                        'name' => 'ConversationID',
                        'value' => $_REQUEST['ConversationID'],
                    ),
                    array (
                        'name' => 'Response',
                        'value' => $_REQUEST['Response'],
                    )
                ),
                'line_items' => $checkout['line_items'],
                'tax_lines' => $checkout['tax_lines'],
                'total_tax' => $checkout['total_tax'],
                'currency' => $checkout['currency'],
                'customer' => array (
                    'first_name' => $checkout['billing_address']['first_name'],
                    'last_name' => $checkout['billing_address']['last_name'],
                    'email' => $checkout['email'],
                ),
                'billing_address' => $checkout['billing_address'],
                'shipping_address' => $checkout['shipping_address'],
                'email' => $checkout['email'],
                'financial_status' => 'pending',
                'gateway' => 'VenuePay'
            )
        );
        $Order = $api->rest('POST', '/admin/orders.json', $orderData);
        echo '<script>window.location.href = "'. $Order->body->order->order_status_url .'"</script>';
    }
}
?>