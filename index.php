<html>
<head>
<title>Hosted Payment</title>
</head>
<body>
<?php
session_start();
require __DIR__.'/vendor/autoload.php';
use Osiset\BasicShopifyAPI;
require __DIR__.'/conf.php';
require __DIR__.'/connect.php';
require __DIR__.'/tempoeConfig.php';

date_default_timezone_set('UTC');

function approveAndLease($token,$response,$shopify,$tempoe,$shop,$checkoutKey) {
	$x_url_callback = explode('/',$shopify['x_url_callback']);
	$endURL = end($x_url_callback);
	$redirectURL = $tempoe->hostURL . '?shop='. $shop . '?key=' . $checkoutKey . '?endURL=' . $endURL;
	$ApproveAndLease_URL = $tempoe->tempoeURL;
	$ApproveAndLease_User = $shopify['x_account_id'];
	$ApproveAndLease_Pass = 'iU44RWxeik';
	$total_price = $response->body->checkout->total_price;
	$shipping_rate = $response->body->checkout->shipping_rate->price;
	$total_tax = $response->body->checkout->total_tax;
	$line_items = $response->body->checkout->line_items;
	$PayDownAmount = $total_price / 6;
	
	$xmlrRoot= new SimpleXMLElement("<WNLILaaSRequest></WNLILaaSRequest>");
	$xmlrWNLILaaSRequest = $xmlrRoot->addChild('LaaSRequest');
	$xmlrWNLILaaSRequest->addChild('LaaSRequestID', 'ApproveAndLease');
	$xmlrWNLILaaSRequest->addChild('ConversationID');
	$xmlrWNLILaaSRequest->addChild('RequestTimeStamp', date("Y-m-d H:i:s"));
	$xmlrWNLILaaSRequest->addChild('UserID', $tempoe->UID);
	$xmlrWNLILaaSRequest->addChild('PSW', $tempoe->PWD);

	$xmlrLaaSConsumerInfo= $xmlrRoot->addChild('LaaSConsumerInfo');
	$xmlrLaaSConsumerInfo->addChild('ConsumerFirstName', $shopify['x_customer_first_name']);
	$xmlrLaaSConsumerInfo->addChild('ConsumerMiddleInitial');
	$xmlrLaaSConsumerInfo->addChild('ConsumerLastName', $shopify['x_customer_last_name']);
	$xmlrLaaSConsumerInfo->addChild('ConsumerPhone',  $shopify['x_customer_phone']);
	$xmlrLaaSConsumerInfo->addChild('ConsumerEmail',  $shopify['x_customer_email']);
	$xmlrLaaSConsumerInfo->addChild('ConsumerAddress1', $shopify['x_customer_billing_address1']);
	if($shopify['x_customer_billing_address2']) {
		$xmlrLaaSConsumerInfo->addChild('ConsumerAddress2', $shopify['x_customer_billing_address2']);
	} else {
		$xmlrLaaSConsumerInfo->addChild('ConsumerAddress2');
	}
	$xmlrLaaSConsumerInfo->addChild('ConsumerCity', $shopify['x_customer_billing_city']);
	$xmlrLaaSConsumerInfo->addChild('ConsumerState', $shopify['x_customer_billing_state']);
	$xmlrLaaSConsumerInfo->addChild('ConsumerZip', $shopify['x_customer_billing_zip']);
	if($shopify['x_customer_billing_address1'] == $shopify['x_customer_shipping_address1']) {
		$xmlrLaaSConsumerInfo->addChild('ShipToSameAsConsumer', "Y");
	} else {
		$xmlrLaaSConsumerInfo->addChild('ShipToSameAsConsumer', "N");
	}
	if ($consumerBasic->ShipToSameAsConsumer == "N"){
		$xmlrLaaSConsumerInfo->addChild('ShipToAddress1', $shopify['x_customer_shipping_address1']);
		$xmlrLaaSConsumerInfo->addChild('ShipToAddress2', $shopify['x_customer_shipping_address2']);
		$xmlrLaaSConsumerInfo->addChild('ShipToCity', $shopify['x_customer_shipping_city']);
		$xmlrLaaSConsumerInfo->addChild('ShipToState', $shopify['x_customer_shipping_state']);
		$xmlrLaaSConsumerInfo->addChild('ShipToZip', $shopify['x_customer_shipping_zip']);
	}
	$xmlrLaaSConsumerInfo->addChild('LanguagePreference');

	$xmlLaaSConsumerPersonalInfo = $xmlrRoot->addChild('LaaSConsumerPersonalInfo');
	$xmlLaaSConsumerPersonalInfo->addChild('ConsumerSSN');
	$xmlLaaSConsumerPersonalInfo->addChild('ConsumerDOB');

	$xmlLaaSLink = $xmlrRoot->addChild('LaaSLink');
	$xmlLaaSLink->addChild('eCommReturnURL', $redirectURL);
	$xmlLaaSLink->addChild('eCommResumeID', $shopify['x_reference']);

	$xmlLaaSTransactionSummary = $xmlrRoot->addChild('LaaSTransactionSummary');
	$xmlLaaSTransactionSummary->addChild('AdditionalPayDownAmount', $PayDownAmount);
	$xmlLaaSTransactionSummary->addChild('SaleAmount', $total_price);
	$xmlLaaSTransactionSummary->addChild('TaxAmount', $total_tax);
	$xmlLaaSTransactionSummary->addChild('TaxableAmount', $total_tax);
	$xmlLaaSTransactionSummary->addChild('NonTaxableAmount', $total_price);
	$xmlLaaSTransactionSummary->addChild('RetailGrouping');
	$xmlLaaSTransactionSummary->addChild('RetailLocID', $tempoe->RetailLocID);
	$xmlLaaSTransactionSummary->addChild('eCommTranID', $shopify['x_reference']);	

	$xmlLaaSTransactionSummary = $xmlrRoot->addChild('LaaSCartInfoExtended');
	$xmlLaaSTransactionSummary->addChild('CartTotal', $total_price);
	$xmlLaaSTransactionSummary->addChild('CartTaxClass');
	$xmlLaaSTransactionSummary->addChild('TaxQuote', 0);
	$xmlLaaSTransactionSummary->addChild('CartTaxTotal', $total_tax);
	$xmlLaaSTransactionSummary->addChild('CartTaxableTotal', $total_tax);
	$xmlLaaSTransactionSummary->addChild('CartNonTaxableTotal', $total_price);

	$xmlLaaSCartItems =  $xmlLaaSTransactionSummary ->addChild('LaaSCartItems');
	foreach( $line_items as $item_id => $item ){
		$xmlLaaSCartItem = $xmlLaaSCartItems->addChild('LaaSCartItem');
		$xmlLaaSCartItem->addChild('ProductCode', $item->sku);
		$xmlLaaSCartItem->addChild('Quantity',  $item->quantity);
		$xmlLaaSCartItem->addChild('ProductDescription', $item->title);
		$xmlLaaSCartItem->addChild('Cost', $item->price);
		$xmlLaaSCartItem->addChild('ProductGroup', $item->vendor);
		$xmlLaaSCartItem->addChild('ProductCategory', $item->vendor);
		$xmlLaaSCartItem->addChild('ProductSubCategory', $item->vendor);
		$xmlLaaSCartItem->addChild('BrandName', $item->vendor);
		$xmlLaaSCartItem->addChild('Devliery');
		$xmlLaaSCartItem->addChild('Notes');
		$xmlLaaSCartItem->addChild('Installation');
		$xmlLaaSCartItem->addChild('Warranty');
		$xmlLaaSCartItem->addChild('Accessories');
		$xmlLaaSCartItem->addChild('TS', date("Y-m-d H:i:s"));
		$xmlLaaSCartItem->addChild('ItemType', $item->title);
		$xmlLaaSCartItem->addChild('ItemExtendedTotal');
		$xmlLaaSCartItem->addChild('ItemTaxQuote');
		$xmlLaaSCartItem->addChild('ItemStateAndLocalFeeQuot');
		$xmlLaaSCartItem->addChild('ItemStateAndLocalFee', 0);
		$xmlLaaSCartItem->addChild('ItemTaxClass');
		$xmlLaaSCartItem->addChild('ItemTaxTotal', $total_tax);
		$xmlLaaSCartItem->addChild('DeliveryDate', date("Y-m-d H:i:s")); //??
	}
	$input_xml = $xmlrRoot->asXML();
	//setting the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $ApproveAndLease_URL);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $input_xml);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/xml',
		'Content-Length: ' . strlen($input_xml))
	);
	$data = curl_exec($ch);
	if ($data === false) {
		throw new Exception(curl_error($ch), curl_errno($ch));
	}
	curl_close($ch);
	return $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
}

if($_REQUEST['x_reference']) {
	echo '<pre>';
	echo 'Hi ' . $_REQUEST['x_customer_first_name'] . ',<br/>';

	$x_url_complete = $_REQUEST['x_url_complete'];
    $urlArr = explode('/',$x_url_complete);
    $shop = $urlArr[2];
    $checkoutKey = $urlArr[5];
	// API
    $api = new BasicShopifyAPI();
    $api->setVersion('unstable');
    $api->setShop($shop);
    $api->setApiKey(SHOPIFY_API_KEY);
    $api->setApiSecret(SHOPIFY_API_SECRET);
	//Access Token
    $select = "SELECT * FROM $tableName WHERE shop='$shop'";
    $result = mysqli_query($con, $select);
    if(mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
		$access_token = $row['access_token'];
        $api->setAccessToken($access_token);
		//checkout API
		$response = $api->rest("GET", "/admin/checkouts/$checkoutKey.json");
		$shopify = $_REQUEST; //This is the request params from shopify.

		// Approve and Lease
		$approveAndLease = approveAndLease($token,$response,$shopify,$tempoe,$shop,$checkoutKey);

		if($approveAndLease['LaaSResponse']['ResponseCode'] == 77) {
			$ConversationID = $approveAndLease['LaaSResponse']['ConversationID'];
			$RedirectURL = $approveAndLease['LaaSResponse']['RedirectURL'];
			header("Location: $RedirectURL", true, 301);
			echo "<script type='text/javascript'>window.location.href='$RedirectURL';</script>";
			exit();
		} else {
			echo 'Try again later!';
            //echo "<script type='text/javascript'>window.location.href='$RedirectURL';</script>";
		}
	}	
	
}
?>
</body>
</html>