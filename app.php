<?php
session_start();
require __DIR__.'/vendor/autoload.php';
use Osiset\BasicShopifyAPI;

require __DIR__.'/conf.php';
require __DIR__.'/connect.php';

$api = new BasicShopifyAPI();
$api->setVersion('unstable'); // "YYYY-MM" or "unstable"
$api->setShop($_GET['shop']);
$api->setApiKey(SHOPIFY_API_KEY);
$api->setApiSecret(SHOPIFY_API_SECRET);

$code = $_GET['code'];
if (!$code) {
    $redirect = $api->getAuthUrl(SHOPIFY_API_SCOPES,SHOPIFY_API_REDIRECT_URI);
    header("Location: {$redirect}");
    exit;
} else {
    $shop = $_GET['shop'];
    $access = $api->requestAccess($code);
    $access_token = $access->access_token;
    $api->setAccessToken($access_token);
    echo $access_token;
    $select = "SELECT * FROM $tableName WHERE shop='$shop'";
    $result = mysqli_query($con, $select);
    if(mysqli_num_rows($result) > 0) {
        $sql = "UPDATE $tableName SET access_token='".$access_token."' WHERE shop='$shop'";
        $update = mysqli_query($con, $sql);
    } else {
        $sql = "INSERT INTO $tableName (access_token, shop) VALUES ('".$access_token."', '".$shop."')";
        $insert = mysqli_query($con, $sql);
    }
}
?>