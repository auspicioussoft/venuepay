<?php
session_start();
require __DIR__.'/vendor/autoload.php';
use Osiset\BasicShopifyAPI;

require __DIR__.'/conf.php';

$api = new BasicShopifyAPI();
$api->setVersion('unstable'); // "YYYY-MM" or "unstable"
$api->setShop($_GET['shop']);
$api->setApiKey(SHOPIFY_API_KEY);
$api->setApiSecret(SHOPIFY_API_SECRET);

$valid = $api->verifyRequest($_GET);
if($valid) {
	$code = $_GET['code'];
	if (!$code) {
		$shop = $_GET['shop'];
	  	$redirect = $api->getAuthUrl(SHOPIFY_API_SCOPES,SHOPIFY_API_REDIRECT_URI);
	  	header("Location: {$redirect}");
	  	exit;
	}
}
?>